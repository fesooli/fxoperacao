package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fxdata.dominio.Endereco;
import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;
import br.com.fxdata.dominio.Manutencao;
import br.com.fxdata.dominio.ManutencaoStatus;

@Controller
public class ManutencaoController {

	Manutencao manutencao;
	List<Estabelecimento> estabelecimentos = new ArrayList<Estabelecimento>();
	List<Manutencao> manutencoes = new ArrayList<Manutencao>();

	ManutencaoController() {
		estabelecimentos.add(new Estabelecimento(1, "Marisa"));
		estabelecimentos.add(new Estabelecimento(2, "Hering"));
		estabelecimentos.add(new Estabelecimento(3, "Boticario"));
		estabelecimentos.add(new Estabelecimento(4, "Cacau Show"));
		estabelecimentos.add(new Estabelecimento(5, "Shopping"));
	}

	@RequestMapping(path = "/listManutencao", method = { RequestMethod.GET })
	public String listarManutencao() {
		return "manutencao/listManutencao";
	}

	@RequestMapping(path = "/cadastrarManutencao", method = { RequestMethod.GET })
	public String cadastrarManutencao(Model model) {
		Manutencao manutencao = new Manutencao();
		manutencao.setEstabelecimento(new Estabelecimento());
		manutencao.getEstabelecimento().setEndereco(new Endereco());
		model.addAttribute("manutencao", manutencao);
		return "manutencao/cadastrarManutencao";
	}

	@RequestMapping(method = { RequestMethod.POST }, value = "manutencao")
	public String cadastrarManutencao(Manutencao manutencao, Model model) {
		Random r = new Random();
		manutencao.setId(r.nextLong());
		manutencao.setStatus(ManutencaoStatus.AGUARDANDOAGENDAMENTO);
		manutencoes.add(manutencao);
		model.addAttribute("manutencoes", manutencoes);
		return "manutencao/listManutencao";
	}

	@RequestMapping(method = { RequestMethod.GET }, path = "/agendarManutencao/{nomeFantasia}/{id}")
	public String agendarManutencao(@PathVariable String nomeFantasia, @PathVariable long id, Model model) {
		Manutencao manutencao = new Manutencao();
		manutencao.setId(id);
		manutencao.setEstabelecimento(new Estabelecimento());
		manutencao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("manutencao", manutencao);
		return "manutencao/agendarManutencao";
	}

	@RequestMapping(method = { RequestMethod.POST }, path = "/agendarManutencao")
	public String agendarManutencao(Model model, Manutencao manutencao) {
		manutencoes.clear();
		manutencao.setStatus(ManutencaoStatus.AGENDADA);
		manutencoes.add(manutencao);
		model.addAttribute("manutencoes", manutencoes);
		return "manutencao/listManutencao";
	}

	@RequestMapping(method = { RequestMethod.GET }, path = "/finalizarManutencao/{nomeFantasia}/{id}")
	public String finalizarManutencao(@PathVariable String nomeFantasia, @PathVariable long id, Model model) {
		Manutencao manutencao = new Manutencao();
		manutencao.setId(id);
		manutencao.setEstabelecimento(new Estabelecimento());
		manutencao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("manutencao", manutencao);
		return "manutencao/finalizarManutencao";
	}

	@RequestMapping(method = { RequestMethod.POST }, path = "/finalizarManutencao")
	public String finalizarManutencao(Model model, Manutencao manutencao, @RequestParam String comment) {
		manutencoes.clear();
		// seta na mão as estruturas que nõ aceitam th:field
		manutencao.setMotivo(comment);//text area, select box e radio button
		manutencao.setStatus(ManutencaoStatus.FINALIZADA);
		manutencoes.add(manutencao);
		model.addAttribute("manutencoes", manutencoes);
		return "manutencao/listManutencao";
	}
	
	@RequestMapping(method = { RequestMethod.GET }, path = "/cancelarManutencao/{nomeFantasia}/{id}")
	public String cancelarManutencao(@PathVariable String nomeFantasia, @PathVariable long id, Model model) {
		Manutencao manutencao = new Manutencao();	
		manutencao.setEstabelecimento(new Estabelecimento());
		manutencao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("manutencao", manutencao);
		return "manutencao/cancelarManutencao";
	}

	@RequestMapping(method = { RequestMethod.POST }, path = "/cancelarManutencao")
	public String cancelarManutencao(Model model, Manutencao manutencao) {
		manutencoes.clear();
		List<Manutencao> manutencoes = new ArrayList<Manutencao>();
		// seta na mão as estruturas que nõ aceitam th:field
		manutencao.setStatus(ManutencaoStatus.CANCELADA);
		manutencoes.add(manutencao);
		model.addAttribute("manutencoes", manutencoes);
		//depois que preenche a tela de cancelar agendamento retorna para a página ListManutencao
		return "manutencao/listManutencao";
	}
	
	@RequestMapping(path = "/manutencoesStatus/{status}", method = { RequestMethod.GET })
	public String manutencoesStatus(@PathVariable String status, Model model) {
		//FAZER A CONSULTA DE INSTALAÇÕES POR STATUS
		
		List<Manutencao> manutencoesStatus = new ArrayList<Manutencao>();
		
		if(!manutencoes.isEmpty()){
			for(Manutencao i : manutencoes){
				if(i.getStatus().toString().equals(status)){
					manutencoesStatus.add(i);
				}
			}
		}
		
		model.addAttribute("manutencoes", manutencoesStatus);
		return "manutencao/listManutencao";
	}
	
	@RequestMapping(path = "/countManutencoes/{status}", method = { RequestMethod.POST })
	public @ResponseBody int countManutencoes(@PathVariable String status, Model model) {
		//FAZER A CONSULTA DE MANUTENÇÕES POR STATUS E CONTAR
		
		List<Manutencao> manutencoesStatus = new ArrayList<Manutencao>();
		
		if(!manutencoes.isEmpty()){
			for(Manutencao i : manutencoes){
				if(i.getStatus().toString().equals(status)){
					manutencoesStatus.add(i);
				}
			}
		}
		
		return manutencoesStatus.size();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getTagss", method = RequestMethod.GET)
	public List<Estabelecimento> getTags(@RequestParam String term) {
		return searchTag(term);
	}

	@ResponseBody
	public List<Estabelecimento> searchTag(String tagName) {
		List<Estabelecimento> resultados = new ArrayList<Estabelecimento>();

		for (Estabelecimento estab : estabelecimentos) {
			if (estab.getNomeEmpresa().contains(tagName)) {
				resultados.add(estab);
			}
		}

		return resultados;
	}
}