package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fxdata.dominio.*;

@Controller
public class UsuarioController {

	List<Usuario> usuarios = new ArrayList<Usuario>();
	
	@RequestMapping(path = "/listUsuarios", method = { RequestMethod.GET })
	public String listUsuarios() {
		return "usuario/listUsuarios";
	}
	
	@RequestMapping(path = "/login", method = { RequestMethod.GET })
	public String login() {
		return "login";
	}
	
	@RequestMapping(path = "/cadastrarUsuario", method = { RequestMethod.GET })
	public String cadastrarUsuario(Model model) {
		Usuario usuario = new Usuario();
		model.addAttribute("usuario", usuario); 
		return "usuario/cadastrarUsuario";
	}
	
	@RequestMapping(path = "/cadastrarUsuario", method = { RequestMethod.POST })
	public String cadastrarUsuario(Model model, Usuario usuario) {
		usuarios.add(usuario);
		model.addAttribute("usuarios", usuarios); 
		return "usuario/listUsuarios";
	}
}
