package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fxdata.dominio.Ambiente;
import br.com.fxdata.dominio.Dispositivo;
import br.com.fxdata.dominio.DispositivoTecnologia;
import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;

@Controller
public class DispositivoController {
	
	List<Instalacao> instalacoes = new ArrayList<Instalacao>();
	List<Dispositivo> dispositivos = new ArrayList<Dispositivo>();

	@RequestMapping(path = "/listarDispositivos", method = { RequestMethod.GET })
	public String listarDispositivos(Model model) {
		model.addAttribute("dispositivos", dispositivos);
		return "dispositivo/listarDispositivos";
	}
	
	@RequestMapping(path = "/cadastrarPeekAmbiente/{nomeFantasia}", method = { RequestMethod.GET })
	public String cadastrarPeekAmbiente(Model model, @PathVariable String nomeFantasia) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao); 
		return "dispositivo/cadastrarPeekAmbiente";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarPeekAmbiente")
	public String cadastrarPeekAmbiente(Instalacao instalacao, Model model, @RequestParam String http,
																					@RequestParam String vnc,
																					@RequestParam String ssh,
																					@RequestParam String webkey,
																					@RequestParam String selectBox)
	{
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setHttp(http);
		dispositivo.setWebkey(webkey);
		dispositivo.setVnc(vnc);
		dispositivo.setSsh(ssh);
		dispositivo.setTecnologia(new DispositivoTecnologia());
		dispositivo.getTecnologia().setNome(selectBox);
		dispositivos.add(dispositivo);			
		
		instalacao.setAmbiente(new Ambiente());
		instalacao.getAmbiente().setAmbientesFilho(new ArrayList<Ambiente>());
		instalacao.getAmbiente().setDispositivosFilho(dispositivos);
		instalacoes.add(instalacao);
		
		model.addAttribute("instalacao", instalacao);
		//model.addAttribute("dispositivo", dispositivo);
		return "ambiente/cadastrarAmbienteInstalacao";
	}
	
	@RequestMapping(path = "/cadastrarPeekboxAmbiente/{nomeFantasia}", method = { RequestMethod.GET })
	public String cadastrarPeekboxAmbiente(Model model, @PathVariable String nomeFantasia) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao); 
		return "dispositivo/cadastrarPeekboxAmbiente";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarPeekboxAmbiente")
	public String cadastrarPeekboxAmbiente(Instalacao instalacao, Model model, @RequestParam String http,
																					@RequestParam String vnc,
																					@RequestParam String ssh,
																					@RequestParam String webkey,
																					@RequestParam String selectBox)
	{
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setHttp(http);
		dispositivo.setWebkey(webkey);
		dispositivo.setVnc(vnc);
		dispositivo.setSsh(ssh);
		dispositivo.setTecnologia(new DispositivoTecnologia());
		dispositivo.getTecnologia().setNome(selectBox);
		dispositivos.add(dispositivo);			
		
		instalacao.setAmbiente(new Ambiente());
		instalacao.getAmbiente().setAmbientesFilho(new ArrayList<Ambiente>());
		instalacao.getAmbiente().setDispositivosFilho(dispositivos);
		instalacoes.add(instalacao);
		
		model.addAttribute("instalacao", instalacao);
		//model.addAttribute("dispositivo", dispositivo);
		return "ambiente/cadastrarAmbienteInstalacao";
	}
	
	@RequestMapping(path = "/cadastrarPeek", method = { RequestMethod.GET })
	public String cadastrarPeek(Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		model.addAttribute("instalacao", instalacao); 
		return "dispositivo/cadastrarPeek";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarPeek")
	public String cadastrarPeek(Instalacao instalacao, Model model, @RequestParam String http,
																					@RequestParam String vnc,
																					@RequestParam String ssh,
																					@RequestParam String webkey,
																					@RequestParam String selectBox)
	{
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setHttp(http);
		dispositivo.setWebkey(webkey);
		dispositivo.setVnc(vnc);
		dispositivo.setSsh(ssh);
		dispositivo.setTecnologia(new DispositivoTecnologia());
		dispositivo.getTecnologia().setNome(selectBox);
		dispositivos.add(dispositivo);
		
		model.addAttribute("dispositivos", dispositivos);
		return "dispositivo/listarDispositivos";
	}
	
	@RequestMapping(path = "/cadastrarPeekbox", method = { RequestMethod.GET })
	public String cadastrarPeekbox(Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		model.addAttribute("instalacao", instalacao); 
		return "dispositivo/cadastrarPeekbox";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarPeekbox")
	public String cadastrarPeekbox(Instalacao instalacao, Model model, @RequestParam String http,
																					@RequestParam String vnc,
																					@RequestParam String ssh,
																					@RequestParam String webkey,
																					@RequestParam String selectBox)
	{
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setHttp(http);
		dispositivo.setWebkey(webkey);
		dispositivo.setVnc(vnc);
		dispositivo.setSsh(ssh);
		dispositivo.setTecnologia(new DispositivoTecnologia());
		dispositivo.getTecnologia().setNome(selectBox);
		dispositivos.add(dispositivo);
		
		model.addAttribute("dispositivos", dispositivos);
		return "dispositivo/listarDispositivos";
	}
}
