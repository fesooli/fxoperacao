package br.com.fxdata.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;

@Controller
public class EstabelecimentoController {
	
	@RequestMapping(path = "/estabelecimento", method = { RequestMethod.GET })
	public String listEstabelecimentos() {
		return "estabelecimento/estabelecimento";
	}
	
	@RequestMapping(path = "/cadastrarEstabInstalacao", method = { RequestMethod.GET })
	public String cadastrarEstabInstalacao(Model model) {
		model.addAttribute("estabelecimento", new Estabelecimento()); 
		return "estabelecimento/cadastrarEstabInstalacao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "cadastrarEstabInstalacao")
	public String cadastrarEstabInstalacao(Estabelecimento estabelecimento, Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(estabelecimento);
		
		
		model.addAttribute("instalacao", instalacao);
		return "instalacao/cadastrarInstalacao";
	}
}
