package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;
import br.com.fxdata.dominio.InstalacaoStatus;

@Controller
public class AgendamentoController {
	
	@RequestMapping(path = "/cancelarAgendamento/{nomeFantasia}", method = { RequestMethod.GET })
	public String cancelarAgendamento(@PathVariable String nomeFantasia, Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/cancelarAgendamento";
	}
	
	@RequestMapping(path = "/cancelarAgendamento", method = { RequestMethod.POST })
	public String cancelarAgendamento(Instalacao instalacao, Model model) {
		instalacao.setStatus(InstalacaoStatus.CANCELADA);		
		List<Instalacao> instalacoes = new ArrayList<Instalacao>();
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}

}
