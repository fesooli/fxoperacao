package br.com.fxdata.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmpresaController {
	
	@RequestMapping(path = "/empresa", method = { RequestMethod.GET })
	public String index() {
		return "empresa";
	}
}
