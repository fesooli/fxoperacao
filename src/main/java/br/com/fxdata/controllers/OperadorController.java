package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fxdata.dominio.Operador;
import br.com.fxdata.dominio.OperadorPerfil;

@Controller
public class OperadorController {
	List<Operador> operadores = new ArrayList<Operador>();

	@RequestMapping(path = "/listOperadores", method = { RequestMethod.GET })
	public String listOperadores() {
		return "operador/listOperadores";
	}

	@RequestMapping(path = "/cadastrarOperador", method = { RequestMethod.GET })
	public String cadastrarOperador(Model model) {
		Operador operador = new Operador();
		model.addAttribute("operador", operador);
		return "operador/cadastrarOperador";
	}

	@RequestMapping(path = "/cadastrarOperador", method = { RequestMethod.POST })
	public String cadastrarOperador(Model model, Operador operador) {
		operador.setPerfil(OperadorPerfil.ADMINISTRADOR);
		operadores.add(operador);
		model.addAttribute("operadores", operadores);
		return "operador/listOperadores";
	}

}
