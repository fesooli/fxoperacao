package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fxdata.dominio.Ambiente;
import br.com.fxdata.dominio.Dispositivo;
import br.com.fxdata.dominio.Endereco;
import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;
import br.com.fxdata.dominio.InstalacaoStatus;
import br.com.fxdata.dominio.Parceiro;

@Controller
public class InstalacaoController {
	
	Instalacao instalacao;
	List<Estabelecimento> estabelecimentos = new ArrayList<Estabelecimento>();
	List<Instalacao> instalacoes = new ArrayList<Instalacao>();
	
	InstalacaoController() {
		estabelecimentos.add(new Estabelecimento(1, "Marisa"));
		estabelecimentos.add(new Estabelecimento(2, "Hering"));
		estabelecimentos.add(new Estabelecimento(3, "Boticario"));
		estabelecimentos.add(new Estabelecimento(4, "Cacau Show"));
		estabelecimentos.add(new Estabelecimento(5, "Shopping"));
	}

	@RequestMapping(path = "/listarInstalacoes", method = { RequestMethod.GET })
	public String listInstalacoes(Model model) {
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(path = "/instalacoesStatus/{status}", method = { RequestMethod.GET })
	public String instalacoesStatus(@PathVariable String status, Model model) {
		//FAZER A CONSULTA DE INSTALAÇÕES POR STATUS
		
		List<Instalacao> instalacoesStatus = new ArrayList<Instalacao>();
		
		if(!instalacoes.isEmpty()){
			for(Instalacao i : instalacoes){
				if(i.getStatus().toString().equals(status)){
					instalacoesStatus.add(i);
				}
			}
		}
		
		model.addAttribute("instalacoes", instalacoesStatus);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(path = "/countInstalacoes/{status}", method = { RequestMethod.POST })
	public @ResponseBody int countInstalacoes(@PathVariable String status, Model model) {
		//FAZER A CONSULTA DE INSTALAÇÕES POR STATUS E CONTAR
		
		List<Instalacao> instalacoesStatus = new ArrayList<Instalacao>();
		
		if(!instalacoes.isEmpty()){
			for(Instalacao i : instalacoes){
				if(i.getStatus().toString().equals(status)){
					instalacoesStatus.add(i);
				}
			}
		}
		
		return instalacoesStatus.size();
	}
	
	@RequestMapping(path = "/cadastrarInstalacoes", method = { RequestMethod.GET })
	public String cadastrarInstalacoes(Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setEndereco(new Endereco());
		model.addAttribute("instalacao", instalacao); 
		return "instalacao/cadastrarInstalacao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "instalacao")
	public String cadastrarInstalacoes(Instalacao instalacao, Model model)
	{
		Random r = new Random();		
		instalacao.setId(r.nextLong());
		instalacao.setStatus(InstalacaoStatus.AGUARDANDOAGENDAMENTO);		
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(method = { RequestMethod.GET }, path = "/agendarInstalacao/{nomeFantasia}/{id}")
	public String agendarInstalacao(@PathVariable String nomeFantasia, @PathVariable long id, Model model)
	{
		Instalacao instalacao = new Instalacao();
		instalacao.setId(id);
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.setParceiro(new Parceiro());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/agendarInstalacao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, path = "/agendarInstalacao")
	public String agendarInstalacao(Model model, Instalacao instalacao)
	{
		instalacoes.clear();
		instalacao.setStatus(InstalacaoStatus.AGENDADA);
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(method = { RequestMethod.GET }, path = "/configurarInstalacao/{nomeFantasia}")
	public String configurarInstalacao(@PathVariable String nomeFantasia, Model model)
	{
		if(model.containsAttribute("instalacao"))
		{
			Instalacao instalacao = new Instalacao();
			instalacao.setEstabelecimento(new Estabelecimento());
			instalacao.setAmbiente(new Ambiente());
			instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
			model.addAttribute("instalacao", instalacao);
			return "instalacao/configurarInstalacao";
		}
		else
		{
			Instalacao instalacao = new Instalacao();
			instalacao.setEstabelecimento(new Estabelecimento());
			instalacao.setAmbiente(new Ambiente());
			instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
			model.addAttribute("instalacao", instalacao);
			return "instalacao/configurarInstalacao";
		}
	}
	
	@RequestMapping(method = { RequestMethod.POST }, path = "/configurarInstalacao")
	public String configurarInstalacao(Model model, Instalacao instalacao,
												@RequestParam String ambientesSelecionados)
	{
		List<Ambiente> ambientesFilho = new ArrayList<Ambiente>();
		Ambiente ambiente = new Ambiente();
		
		if(ambientesSelecionados.contains(",")){
			String[] nomeAmbiente  = ambientesSelecionados.split(",");
			
			for(int i = 0; i < nomeAmbiente.length; i++){
				ambiente.setNome(nomeAmbiente[i]);
				ambientesFilho.add(ambiente);
			}
		}
		else
		{
			ambiente.setNome(ambientesSelecionados);
			ambientesFilho.add(ambiente);
		}
					
		instalacoes.clear();
		Random r = new Random();		
		instalacao.setId(r.nextLong());
		instalacao.setAmbiente(ambiente);
		instalacao.setStatus(InstalacaoStatus.AGUARDANDOCALIBRAGEM);
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(method = { RequestMethod.GET }, path = "/cancelarCalibracao/{nomeFantasia}")
	public String cancelarCalibracao(@PathVariable String nomeFantasia, Model model)
	{
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/cancelarCalibracao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, path = "/cancelarCalibracao")
	public String cancelarCalibracao(Model model, Instalacao instalacao)
	{
		instalacoes.clear();
		Random r = new Random();		
		instalacao.setId(r.nextLong());
		instalacao.setStatus(InstalacaoStatus.AGUARDANDOAGENDAMENTO);
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}

	@RequestMapping(method = { RequestMethod.GET }, path = "/calibracaoExecutada/{nomeFantasia}")
	public String calibracaoExecutada(@PathVariable String nomeFantasia, Model model)
	{
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/calibracaoExecutada";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, path = "/calibracaoExecutada")
	public String calibracaoExecutada(Model model, Instalacao instalacao)
	{
		instalacoes.clear();
		Random r = new Random();		
		instalacao.setId(r.nextLong());
		instalacao.setStatus(InstalacaoStatus.EMVALIDAÇÃO);
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(method = { RequestMethod.GET }, path = "/cancelarValidacao/{nomeFantasia}")
	public String cancelarValidacao(@PathVariable String nomeFantasia, Model model)
	{
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/cancelarValidacao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, path = "/cancelarValidacao")
	public String cancelarValidacao(Model model, Instalacao instalacao)
	{
		instalacoes.clear();
		Random r = new Random();		
		instalacao.setId(r.nextLong());
		instalacao.setStatus(InstalacaoStatus.AGUARDANDOCALIBRAGEM);
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	@RequestMapping(method = { RequestMethod.GET }, path = "/concluirValidacao/{nomeFantasia}")
	public String concluirValidacao(@PathVariable String nomeFantasia, Model model)
	{
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/validacaoConcluida";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, path = "/concluirValidacao")
	public String concluirValidacao(Model model, Instalacao instalacao)
	{
		instalacoes.clear();
		Random r = new Random();		
		instalacao.setId(r.nextLong());
		instalacao.setStatus(InstalacaoStatus.PRONTA);
		instalacoes.add(instalacao);
		model.addAttribute("instalacoes", instalacoes);
		return "instalacao/listInstalacoes";
	}
	
	
	@RequestMapping(value = "/getNomeEstabelcimento", method = RequestMethod.GET)
	public @ResponseBody List<Estabelecimento> getNomeEstabelcimento(@RequestParam String term)
	{
		List<Estabelecimento> resultados = new ArrayList<Estabelecimento>();
		
		for(Estabelecimento estab : estabelecimentos)
		{
			if (estab.getNomeEmpresa().contains(term)) {
				resultados.add(estab);
			}
		}
		
		return resultados;
	}
}
