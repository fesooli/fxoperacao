package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fxdata.dominio.Endereco;
import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;
import br.com.fxdata.dominio.InstalacaoStatus;
import br.com.fxdata.dominio.Manutencao;
import br.com.fxdata.dominio.Parceiro;

@Controller
public class ParceiroController {
	
	List<Parceiro> parceiros = new ArrayList<Parceiro>();
	List<Parceiro> parcs = new ArrayList<Parceiro>();
	
	ParceiroController() {
		parceiros.add(new Parceiro(1, "Marisa"));
		parceiros.add(new Parceiro(2, "Hering"));
		parceiros.add(new Parceiro(3, "Boticario"));
		parceiros.add(new Parceiro(4, "Cacau Show"));
		parceiros.add(new Parceiro(5, "Shopping"));
	}
	
	@RequestMapping(path = "/listarParceiros", method = { RequestMethod.GET })
	public String listarParceiros(Model model) {
		model.addAttribute("instalacoes", parcs);
		return "parceiro/listarParceiros";
	}
	
	@RequestMapping(path = "/cadastrarParceiro", method = { RequestMethod.GET })
	public String cadastrarParceiro(Model model) {
		Parceiro parceiro = new Parceiro();
		model.addAttribute("parceiro", parceiro); 
		return "parceiro/cadastrarParceiro";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarParceiro")
	public String cadastrarParceiro(Parceiro parceiro, Model model)
	{
		Random r = new Random();		
		parceiro.setId(r.nextLong());	
		parcs.add(parceiro);
		model.addAttribute("parceiros", parcs);
		return "parceiro/listarParceiros";
	}
	
	@RequestMapping(path = "/cadastrarParceiroInstalacao/{nomeFantasia}", method = { RequestMethod.GET })
	public String cadastrarParceiroInstalacao(Model model, @PathVariable String nomeFantasia) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		instalacao.setParceiro(new Parceiro());
		model.addAttribute("instalacao", instalacao); 
		return "parceiro/cadastrarParceiroInstalacao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarParceiroInstalacao")
	public String cadastrarParceiroInstalacao(Instalacao instalacao, Model model)
	{
		model.addAttribute("instalacao", instalacao);
		return "instalacao/agendarInstalacao";
	}
	
	@RequestMapping(path = "/cadastrarParceiroManutencao/{nomeFantasia}", method = { RequestMethod.GET })
	public String cadastrarParceiroManutencao(Model model, @PathVariable String nomeFantasia) {
		Manutencao manutencao = new Manutencao();
		manutencao.setEstabelecimento(new Estabelecimento());
		manutencao.getEstabelecimento().setNomeFantasia(nomeFantasia);
		manutencao.setParceiro(new Parceiro());
		model.addAttribute("manutencao", manutencao); 
		return "parceiro/cadastrarParceiroManutencao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "/cadastrarParceiroManutencao")
	public String cadastrarParceiroManutencao(Manutencao manutencao, Model model)
	{
		model.addAttribute("manutencao", manutencao);
		return "manutencao/agendarManutencao";
	}
	
	@RequestMapping(value = "/getNomeParceiro", method = RequestMethod.GET)
	public @ResponseBody List<Parceiro> getNomeParceiro(@RequestParam String term)
	{
		List<Parceiro> resultados = new ArrayList<Parceiro>();
		
		for(Parceiro parceiro : parceiros)
		{
			if (parceiro.getNome().contains(term)) {
				resultados.add(parceiro);
			}
		}
		
		return resultados;
	}

}
