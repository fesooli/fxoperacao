package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fxdata.dominio.Ambiente;
import br.com.fxdata.dominio.Dispositivo;
import br.com.fxdata.dominio.DispositivoTecnologia;
import br.com.fxdata.dominio.Endereco;
import br.com.fxdata.dominio.Estabelecimento;
import br.com.fxdata.dominio.Instalacao;
import br.com.fxdata.dominio.InstalacaoStatus;

@Controller
public class AmbienteController {
	
	List<Ambiente> ambientesFilho = new ArrayList<Ambiente>();

	@RequestMapping(path = "/listAmbiente", method = { RequestMethod.GET })
	public String listUsuarios() {
		return "ambiente/listAmbiente";
	}
	
	@RequestMapping(path = "/cadastrarAmbienteInstalacao/{nomeFantasia}", method = { RequestMethod.GET })
	public String cadastrarAmbienteInstalacao(Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.setAmbiente(new Ambiente());
		instalacao.getAmbiente().setDispositivosFilho(new ArrayList<Dispositivo>());
		model.addAttribute("instalacao", instalacao); 
		return "ambiente/cadastrarAmbienteInstalacao";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "cadastrarAmbienteInstalacao")
	public String cadastrarAmbienteInstalacao(Instalacao instalacao, Model model, 
																	@RequestParam String nomeAmbienteFilho,
																	@RequestParam String selecionados)
	{
		Ambiente ambiente = new Ambiente();
		ambiente.setNome(nomeAmbienteFilho);
		
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setTecnologia(new DispositivoTecnologia());
		List<Dispositivo> dispositivosFilho = new ArrayList<Dispositivo>();		
		
		if(selecionados.contains(",")){
			String[] nomeDispositivo  = selecionados.split(",");
			
			for(int i = 0; i < nomeDispositivo.length; i++){
				dispositivo.getTecnologia().setNome(nomeDispositivo[i]);
				dispositivosFilho.add(dispositivo);
			}
		}
		
		ambiente.setDispositivosFilho(dispositivosFilho);
		ambientesFilho.add(ambiente);
		instalacao.setAmbiente(ambiente);
		instalacao.getAmbiente().setAmbientesFilho(ambientesFilho);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/configurarInstalacao";
	}
	
	@RequestMapping(path = "/cadastrarAmbiente", method = { RequestMethod.GET })
	public String cadastrarAmbiente(Model model) {
		Instalacao instalacao = new Instalacao();
		instalacao.setEstabelecimento(new Estabelecimento());
		instalacao.setAmbiente(new Ambiente());
		instalacao.getAmbiente().setDispositivosFilho(new ArrayList<Dispositivo>());
		model.addAttribute("instalacao", instalacao); 
		return "ambiente/cadastrarAmbiente";
	}
	
	@RequestMapping(method = { RequestMethod.POST }, value = "cadastrarAmbiente")
	public String cadastrarAmbiente(Instalacao instalacao, Model model, 
																	@RequestParam String nomeAmbienteFilho,
																	@RequestParam String selecionados)
	{
		Ambiente ambiente = new Ambiente();
		ambiente.setNome(nomeAmbienteFilho);
		
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setTecnologia(new DispositivoTecnologia());
		List<Dispositivo> dispositivosFilho = new ArrayList<Dispositivo>();		
		
		if(selecionados.contains(",")){
			String[] nomeDispositivo  = selecionados.split(",");
			
			for(int i = 0; i < nomeDispositivo.length; i++){
				dispositivo.getTecnologia().setNome(nomeDispositivo[i]);
				dispositivosFilho.add(dispositivo);
			}
		}
		
		ambiente.setDispositivosFilho(dispositivosFilho);
		ambientesFilho.add(ambiente);
		instalacao.setAmbiente(ambiente);
		instalacao.getAmbiente().setAmbientesFilho(ambientesFilho);
		model.addAttribute("instalacao", instalacao);
		return "instalacao/configurarInstalacao";
	}
}
