package br.com.fxdata.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fxdata.dominio.Eventos;

@Controller
public class PainelController {

	@RequestMapping(path = "/painelInstalacao", method = { RequestMethod.GET })
	public String abrirPainelInstalacao() {
		return "painel/painelInstalacao";
	}
	
	@RequestMapping(path = "/painelManutencao", method = { RequestMethod.GET })
	public String abrirPainelManutencao() {
		return "painel/painelManutencao";
	}
	
	@RequestMapping(path = "/agenda", method = { RequestMethod.GET })
	public String agenda() {
		return "painel/calendar";
	}
	
	@RequestMapping(path = "/mapaClientes", method = { RequestMethod.GET })
	public String mapaClientes() {
		return "painel/mapaClientes";
	}
	
	@RequestMapping(path = "/getAgenda.json", method = { RequestMethod.GET })
	public @ResponseBody List<Eventos> atualizarAgenda() {
		List<Eventos> eventos = new ArrayList<Eventos>();
		
		String mesAtual = String.valueOf(Calendar.getInstance().get(Calendar.MONTH)+ 1);
		
		if(mesAtual.length() <2)
			mesAtual = "0" + mesAtual;
		
		String anoAtual = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
				
		/*ADICIONANDO OS EVENTOS*/
		eventos.add(new Eventos("Estudar Linux",     anoAtual+"-"+mesAtual+"-02T12:00:00",anoAtual+"-"+mesAtual+"-02T13:30:00", null));
		eventos.add(new Eventos("Estudar Java", 	   anoAtual+"-"+mesAtual+"-02T14:00:00",anoAtual+"-"+mesAtual+"-02T14:30:00", null));
		eventos.add(new Eventos("Estudar C#", 	   anoAtual+"-"+mesAtual+"-02T15:00:00",anoAtual+"-"+mesAtual+"-02T15:30:00", null));
		eventos.add(new Eventos("Estudar SOA Suite", anoAtual+"-"+mesAtual+"-02T16:00:00",anoAtual+"-"+mesAtual+"-02T17:30:00", null));
		eventos.add(new Eventos("Estudar Jquery",    anoAtual+"-"+mesAtual+"-02T19:00:00",anoAtual+"-"+mesAtual+"-02T20:30:00", null));		
		eventos.add(new Eventos("Correr",     	   anoAtual+"-"+mesAtual+"-03T13:00:00",anoAtual+"-"+mesAtual+"-03T13:30:00", null));
		eventos.add(new Eventos("Reunião",	       anoAtual+"-"+mesAtual+"-05T12:00:00",anoAtual+"-"+mesAtual+"-05T13:30:00", null));		
		eventos.add(new Eventos("Dois dias de evento", anoAtual+"-"+mesAtual+"-07T12:00:00",anoAtual+"-"+mesAtual+"-08T12:00:00", null));
		
		eventos.add(new Eventos("Publicar Artigo",   anoAtual+"-"+mesAtual+"-10T12:00:00",anoAtual+"-"+mesAtual+"-10T13:30:00", null));
		eventos.add(new Eventos("Reunião",	       anoAtual+"-"+mesAtual+"-10T15:00:00",anoAtual+"-"+mesAtual+"-10T18:30:00", null));		
		
		eventos.add(new Eventos("Festa",  		   anoAtual+"-"+mesAtual+"-13T12:00:00",anoAtual+"-"+mesAtual+"-13T13:30:00", null));
		eventos.add(new Eventos("Festa 2",	       anoAtual+"-"+mesAtual+"-13T15:00:00",anoAtual+"-"+mesAtual+"-13T18:30:00", null));		
		eventos.add(new Eventos("Curso de Inglês",   anoAtual+"-"+mesAtual+"-15",null, null));				
		eventos.add(new Eventos("Blog Cícero",       anoAtual+"-"+mesAtual+"-23",null, "http://www.ciceroednilson.com.br"));
				
		return eventos;
	}
	
	@RequestMapping(path = "/getMapa.json", method = { RequestMethod.GET })
	public @ResponseBody List<String> atualizarMapa() {
		List<String> enderecos = new ArrayList<String>();
		
		enderecos.add("Rua Claudio da Costa - São Paulo");
		enderecos.add("Vila Olimpia - São Paulo");
		enderecos.add("Mogi das Cruzes");
		enderecos.add("Ferraz de Vasconcelos");
		enderecos.add("Suzano");
		
		return enderecos;
	}
}
