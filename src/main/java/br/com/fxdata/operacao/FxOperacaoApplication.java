package br.com.fxdata.operacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "br.com")
@EnableAutoConfiguration
public class FxOperacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FxOperacaoApplication.class, args);
	}
}
