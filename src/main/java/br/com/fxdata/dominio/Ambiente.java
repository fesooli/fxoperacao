package br.com.fxdata.dominio;

import java.util.List;

public class Ambiente {

	private Long id;

	private String nome;

	private AmbienteCategoria categoria;

	private List<Ambiente> ambientesFilho;

	private List<Dispositivo> dispositivosFilho;

	private AmbienteStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public AmbienteCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(AmbienteCategoria categoria) {
		this.categoria = categoria;
	}

	public List<Ambiente> getAmbientesFilho() {
		return ambientesFilho;
	}

	public void setAmbientesFilho(List<Ambiente> ambientesFilho) {
		this.ambientesFilho = ambientesFilho;
	}

	public List<Dispositivo> getDispositivosFilho() {
		return dispositivosFilho;
	}

	public void setDispositivosFilho(List<Dispositivo> dispositivosFilho) {
		this.dispositivosFilho = dispositivosFilho;
	}

	public AmbienteStatus getStatus() {
		return status;
	}

	public void setStatus(AmbienteStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Ambiente [id=" + id + ", nome=" + nome + ", categoria=" + categoria + ", status=" + status + "]";
	}

}
