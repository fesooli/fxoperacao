package br.com.fxdata.dominio;


public class ContatoResponsavel {

	private String nomeResponsavel;
	private String telefoneResponsavel;
	private String celularResponsavel;
	private String emailResponsavel;

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public String getTelefoneResponsavel() {
		return telefoneResponsavel;
	}

	public void setTelefoneResponsavel(String telefoneResponsavel) {
		this.telefoneResponsavel = telefoneResponsavel;
	}

	public String getCelularResponsavel() {
		return celularResponsavel;
	}

	public void setCelularResponsavel(String celularResponsavel) {
		this.celularResponsavel = celularResponsavel;
	}

	public String getEmailResponsavel() {
		return emailResponsavel;
	}

	public void setEmailResponsavel(String emailResponsavel) {
		this.emailResponsavel = emailResponsavel;
	}

	@Override
	public String toString() {
		return "ContatoResponsavel [nomeResponsavel=" + nomeResponsavel + ", telefoneResponsavel=" + telefoneResponsavel
				+ ", celularResponsavel=" + celularResponsavel + ", emailResponsavel=" + emailResponsavel + "]";
	}

}
