package br.com.fxdata.dominio;

import java.util.List;

public class Usuario {

	private Long id;
	private String email;
	private String nome;
	private String senhaMD5;

	private AmbienteCategoria raizVisao;

	private UsuarioPerfil perfil;

	private UsuarioStatus status;

	private List<Ambiente> ambientes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenhaMD5() {
		return senhaMD5;
	}

	public void setSenhaMD5(String senhaMD5) {
		this.senhaMD5 = senhaMD5;
	}

	public AmbienteCategoria getRaizVisao() {
		return raizVisao;
	}

	public void setRaizVisao(AmbienteCategoria raizVisao) {
		this.raizVisao = raizVisao;
	}

	public UsuarioPerfil getPerfil() {
		return perfil;
	}

	public void setPerfil(UsuarioPerfil perfil) {
		this.perfil = perfil;
	}

	public UsuarioStatus getStatus() {
		return status;
	}

	public void setStatus(UsuarioStatus status) {
		this.status = status;
	}

	public List<Ambiente> getAmbientes() {
		return ambientes;
	}

	public void setAmbientes(List<Ambiente> ambientes) {
		this.ambientes = ambientes;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", email=" + email + ", nome=" + nome + ", senhaMD5=" + senhaMD5 + ", raizVisao="
				+ raizVisao + ", perfil=" + perfil + ", status=" + status + "]";
	}

}
