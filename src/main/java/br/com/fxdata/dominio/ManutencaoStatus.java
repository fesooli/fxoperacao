package br.com.fxdata.dominio;

public enum ManutencaoStatus {

	AGENDADA, AGUARDANDOAGENDAMENTO,FINALIZADA,CANCELADA
}
