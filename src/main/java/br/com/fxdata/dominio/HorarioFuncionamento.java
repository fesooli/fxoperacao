package br.com.fxdata.dominio;

import java.util.Date;

public class HorarioFuncionamento {

	private Long id;

	private Integer diaSemana;

	private Date horarioInicio;

	private Date horarioFim;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(Integer diaSemana) {
		this.diaSemana = diaSemana;
	}

	public Date getHorarioInicio() {
		return horarioInicio;
	}

	public void setHorarioInicio(Date horarioInicio) {
		this.horarioInicio = horarioInicio;
	}

	public Date getHorarioFim() {
		return horarioFim;
	}

	public void setHorarioFim(Date horarioFim) {
		this.horarioFim = horarioFim;
	}

	@Override
	public String toString() {
		return "HorarioFuncionamento [id=" + id + ", diaSemana=" + diaSemana + ", horarioInicio=" + horarioInicio
				+ ", horarioFim=" + horarioFim + "]";
	}

}
