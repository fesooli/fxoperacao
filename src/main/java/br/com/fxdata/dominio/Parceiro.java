package br.com.fxdata.dominio;

public class Parceiro {
	
	long id;
	String nome;
	
	public Parceiro(int id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Parceiro(){
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
