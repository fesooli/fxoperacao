package br.com.fxdata.dominio;

import java.util.Date;

public class Manutencao {
	
	private long id;
	private ManutencaoStatus status;
	private Estabelecimento estabelecimento;
	private Parceiro parceiro;
	private Date dataManutencao;
	private String execucao;
	private String motivo;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	
	/**
	 * @return the estabelecimento
	 */
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	/**
	 * @param estabelecimento the estabelecimento to set
	 */
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	/**
	 * @return the parceiro
	 */
	public Parceiro getParceiro() {
		return parceiro;
	}
	/**
	 * @param parceiro the parceiro to set
	 */
	public void setParceiro(Parceiro parceiro) {
		this.parceiro = parceiro;
	}
	/**
	 * @return the dataManutencao
	 */
	public Date getDataManutencao() {
		return dataManutencao;
	}
	/**
	 * @param dataManutencao the dataManutencao to set
	 */
	public void setDataManutencao(Date dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	/**
	 * @return the status
	 */
	public ManutencaoStatus getStatus() {
		return status;
	}
	/**
	 * @return the execucao
	 */
	public String getExecucao() {
		return execucao;
	}
	/**
	 * @param execucao the execucao to set
	 */
	public void setExecucao(String execucao) {
		this.execucao = execucao;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(ManutencaoStatus status) {
		this.status = status;
	}
	
	

	
}