package br.com.fxdata.dominio;

public class Operador {
private Long id;
 private String nome;
 private String sobrenome;
 private String email; 
 private String senhaMD5;
 
 private OperadorPerfil perfil;
/**
 * @return the nome
 */
public String getNome() {
	return nome;
}
/**
 * @param nome the nome to set
 */
public void setNome(String nome) {
	this.nome = nome;
}
/**
 * @return the email
 */
public String getEmail() {
	return email;
}
/**
 * @param email the email to set
 */
public void setEmail(String email) {
	this.email = email;
}
/**
 * @return the senhaMD5
 */
public String getSenhaMD5() {
	return senhaMD5;
}
/**
 * @param senhaMD5 the senhaMD5 to set
 */
public void setSenhaMD5(String senhaMD5) {
	this.senhaMD5 = senhaMD5;
}
/**
 * @return the id
 */
public Long getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(Long id) {
	this.id = id;
}
/**
 * @return the sobrenome
 */
public String getSobrenome() {
	return sobrenome;
}
/**
 * @param sobrenome the sobrenome to set
 */
public void setSobrenome(String sobrenome) {
	this.sobrenome = sobrenome;
}
/**
 * @return the perfil
 */
public OperadorPerfil getPerfil() {
	return perfil;
}
/**
 * @param perfil the perfil to set
 */
public void setPerfil(OperadorPerfil perfil) {
	this.perfil = perfil;
}
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
	return "Operador [id=" + id + ", nome=" + nome + ", sobrenome=" + sobrenome + ", email=" + email + ", senhaMD5="
			+ senhaMD5 + ", perfil=" + perfil + "]";
}

 
}
