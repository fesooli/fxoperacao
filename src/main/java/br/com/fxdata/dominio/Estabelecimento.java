package br.com.fxdata.dominio;

import java.util.List;

import br.com.fxdata.dominio.HorarioFuncionamento;

public class Estabelecimento {

	private long id;
	private String nomeEmpresa;
	private String nomeFantasia;
	private String cnpj;
	private String razaoSocial;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;
	private int qtdePeeks;
	private int qtdePeekBoxes;
	private Endereco endereco;
	private EstabelecimentoStatus status;
	private List<HorarioFuncionamento> horariosFuncionamento;
	private ContatoResponsavel contatoResponsavel;	
	
	public EstabelecimentoStatus getStatus() {
		return status;
	}

	public void setStatus(EstabelecimentoStatus status) {
		this.status = status;
	}
	
	public ContatoResponsavel getContatoResponsavel() {
		return contatoResponsavel;
	}

	public void setContatoResponsavel(ContatoResponsavel contatoResponsavel) {
		this.contatoResponsavel = contatoResponsavel;
	}

	public Estabelecimento(int id, String nomeEmpresa)
	{
		this.id = id;
		this.nomeEmpresa = nomeEmpresa;
	}
	
	public Estabelecimento()
	{
	}
		
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<HorarioFuncionamento> getHorariosFuncionamento() {
		return horariosFuncionamento;
	}

	public void setHorariosFuncionamento(List<HorarioFuncionamento> horariosFuncionamento) {
		this.horariosFuncionamento = horariosFuncionamento;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	public void setNome(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public int getQtdePeeks() {
		return qtdePeeks;
	}

	public void setQtdePeeks(int qtdePeeks) {
		this.qtdePeeks = qtdePeeks;
	}

	public int getQtdePeekBoxes() {
		return qtdePeekBoxes;
	}

	public void setQtdePeekBoxes(int qtdePeekBoxes) {
		this.qtdePeekBoxes = qtdePeekBoxes;
	}
	
	
}
