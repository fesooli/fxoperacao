package br.com.fxdata.dominio;

public class Dispositivo {

	private Long id;

	private String identficadorFisico;
	private DispositivoStatus status;
	private DispositivoTecnologia tecnologia;
	private String http;
	private String ssh;
	private String vnc;
	private String webkey;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentficadorFisico() {
		return identficadorFisico;
	}

	public void setIdentficadorFisico(String identficadorFisico) {
		this.identficadorFisico = identficadorFisico;
	}

	public DispositivoStatus getStatus() {
		return status;
	}

	public void setStatus(DispositivoStatus status) {
		this.status = status;
	}

	public DispositivoTecnologia getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(DispositivoTecnologia tecnlogia) {
		this.tecnologia = tecnlogia;
	}

	@Override
	public String toString() {
		return "Dispositivo [id=" + id + ", identficadorFisico=" + identficadorFisico + ", status=" + status
				+ ", tecnologia=" + tecnologia + "]";
	}

	public String getHttp() {
		return http;
	}

	public void setHttp(String http) {
		this.http = http;
	}

	public String getSsh() {
		return ssh;
	}

	public void setSsh(String ssh) {
		this.ssh = ssh;
	}

	public String getVnc() {
		return vnc;
	}

	public void setVnc(String vnc) {
		this.vnc = vnc;
	}

	public String getWebkey() {
		return webkey;
	}

	public void setWebkey(String webkey) {
		this.webkey = webkey;
	}

}
