package br.com.fxdata.dominio;

public class Cidade {

	private Long id;

	private String nome;

	private EstadoUF uf;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadoUF getUf() {
		return uf;
	}

	public void setUf(EstadoUF uf) {
		this.uf = uf;
	}

	@Override
	public String toString() {
		return "Cidade [id=" + id + ", nome=" + nome + ", uf=" + uf + "]";
	}

}
